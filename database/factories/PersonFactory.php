<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Person::class, function (Faker $faker) {
	$genders = ['male', 'female'];
	$gender = $genders[array_rand($genders, 1)];
	$firstName = $gender == 'male' ? 'firstNameMale' : 'firstNameFemale';
    return [
       'gender' => $gender,
       'first_name' => $faker->$firstName,
       'last_name' => $faker->lastName,
       'address' => $faker->address
    ];
});
